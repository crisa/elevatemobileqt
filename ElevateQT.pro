#-------------------------------------------------
#  Copyright 2016 ESRI
#
#  All rights reserved under the copyright laws of the United States
#  and applicable international laws, treaties, and conventions.
#
#  You may freely redistribute and use this sample code, with or
#  without modification, provided you include the original copyright
#  notice and use restrictions.
#
#  See the Sample code usage restrictions document for further information.
#-------------------------------------------------

mac {
    cache()
}

#-------------------------------------------------------------------------------

CONFIG += c++11 arcgis_runtime_qml100_0_0

QT += core gui opengl xml network positioning sensors
QT += qml quick

include(quickandroid/quickandroid.pri)

TEMPLATE = app
TARGET = ElevateQT

#-------------------------------------------------------------------------------

HEADERS +=     AppInfo.h

SOURCES +=     main.cpp

RESOURCES +=     qml/qml.qrc     Resources/Resources.qrc


OTHER_FILES +=     wizard.xml     wizard.png

#-------------------------------------------------------------------------------

win32 {
    include (Win/Win.pri)
}

macx {
    include (Mac/Mac.pri)
}

ios {
    include (iOS/iOS.pri)
}

android {
    QT += androidextras
    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/Android
    include (Android/Android.pri)
}

DISTFILES += \
    quickandroid/QuickAndroid/Private/incubator.js \
    quickandroid/QuickAndroid/Styles/utils.js \
    quickandroid/QuickAndroid/global.js \
    quickandroid/QuickAndroid/utils.js \
    quickandroid/qpm.json \
    quickandroid/examples/quickandroidexample/android-sources/AndroidManifest.xml \
    quickandroid/tests/quickandroidactivitytests/tests/AndroidManifest.xml \
    quickandroid/examples/quickandroidexample/deployment.pri \
    quickandroid/quickandroid.pri \
    quickandroid/examples/quickandroidexample/android-sources/gradle/wrapper/gradle-wrapper.jar \
    quickandroid/.travis/deploy_pages.sh \
    quickandroid/examples/quickandroidexample/android-sources/gradlew \
    quickandroid/tests/quickandroidactivitytests/run-build.sh \
    quickandroid/tests/quickandroidactivitytests/run-tests.sh \
    quickandroid/.travis.yml \
    quickandroid/examples/quickandroidexample/android-sources/res/drawable/splash.xml \
    quickandroid/examples/quickandroidexample/android-sources/res/values/apptheme.xml \
    quickandroid/examples/quickandroidexample/android-sources/res/values/libs.xml \
    Android/AndroidManifest.xml \
    Android/Android.pri \
    Android/res/drawable-hdpi/icon.png \
    Android/res/drawable-ldpi/icon.png \
    Android/res/drawable-mdpi/icon.png \
    Android/res/drawable-xhdpi/icon.png \
    Android/res/drawable-xxhdpi/icon.png \
    Android/res/values/libs.xml \
    Android/build.gradle \
    Android/gradle/wrapper/gradle-wrapper.jar \
    Android/gradlew \
    Android/gradle/wrapper/gradle-wrapper.properties \
    Android/gradlew.bat


