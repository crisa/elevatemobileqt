/****************************************************************************
** Meta object code from reading C++ file 'qaimagewriter.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "quickandroid/qaimagewriter.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qaimagewriter.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QAImageWriter_t {
    QByteArrayData data[12];
    char stringdata0[108];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QAImageWriter_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QAImageWriter_t qt_meta_stringdata_QAImageWriter = {
    {
QT_MOC_LITERAL(0, 0, 13), // "QAImageWriter"
QT_MOC_LITERAL(1, 14, 14), // "runningChanged"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 14), // "fileUrlChanged"
QT_MOC_LITERAL(4, 45, 12), // "imageChanged"
QT_MOC_LITERAL(5, 58, 8), // "finished"
QT_MOC_LITERAL(6, 67, 4), // "save"
QT_MOC_LITERAL(7, 72, 7), // "fileUrl"
QT_MOC_LITERAL(8, 80, 5), // "clear"
QT_MOC_LITERAL(9, 86, 7), // "endSave"
QT_MOC_LITERAL(10, 94, 7), // "running"
QT_MOC_LITERAL(11, 102, 5) // "image"

    },
    "QAImageWriter\0runningChanged\0\0"
    "fileUrlChanged\0imageChanged\0finished\0"
    "save\0fileUrl\0clear\0endSave\0running\0"
    "image"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QAImageWriter[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       3,   66, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x06 /* Public */,
       3,    0,   55,    2, 0x06 /* Public */,
       4,    0,   56,    2, 0x06 /* Public */,
       5,    0,   57,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    1,   58,    2, 0x0a /* Public */,
       6,    0,   61,    2, 0x2a /* Public | MethodCloned */,
       8,    0,   62,    2, 0x0a /* Public */,
       9,    1,   63,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    7,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    7,

 // properties: name, type, flags
      10, QMetaType::Bool, 0x00495103,
       7, QMetaType::QString, 0x00495103,
      11, QMetaType::QImage, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,

       0        // eod
};

void QAImageWriter::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QAImageWriter *_t = static_cast<QAImageWriter *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->runningChanged(); break;
        case 1: _t->fileUrlChanged(); break;
        case 2: _t->imageChanged(); break;
        case 3: _t->finished(); break;
        case 4: _t->save((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: _t->save(); break;
        case 6: _t->clear(); break;
        case 7: _t->endSave((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QAImageWriter::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAImageWriter::runningChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QAImageWriter::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAImageWriter::fileUrlChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QAImageWriter::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAImageWriter::imageChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QAImageWriter::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QAImageWriter::finished)) {
                *result = 3;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QAImageWriter *_t = static_cast<QAImageWriter *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->running(); break;
        case 1: *reinterpret_cast< QString*>(_v) = _t->fileUrl(); break;
        case 2: *reinterpret_cast< QImage*>(_v) = _t->image(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QAImageWriter *_t = static_cast<QAImageWriter *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setRunning(*reinterpret_cast< bool*>(_v)); break;
        case 1: _t->setFileUrl(*reinterpret_cast< QString*>(_v)); break;
        case 2: _t->setImage(*reinterpret_cast< QImage*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject QAImageWriter::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QAImageWriter.data,
      qt_meta_data_QAImageWriter,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QAImageWriter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QAImageWriter::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QAImageWriter.stringdata0))
        return static_cast<void*>(const_cast< QAImageWriter*>(this));
    return QObject::qt_metacast(_clname);
}

int QAImageWriter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QAImageWriter::runningChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QAImageWriter::fileUrlChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QAImageWriter::imageChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QAImageWriter::finished()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
