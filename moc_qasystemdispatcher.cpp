/****************************************************************************
** Meta object code from reading C++ file 'qasystemdispatcher.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "quickandroid/qasystemdispatcher.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qasystemdispatcher.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QASystemDispatcher_t {
    QByteArrayData data[8];
    char stringdata0[77];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QASystemDispatcher_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QASystemDispatcher_t qt_meta_stringdata_QASystemDispatcher = {
    {
QT_MOC_LITERAL(0, 0, 18), // "QASystemDispatcher"
QT_MOC_LITERAL(1, 19, 10), // "dispatched"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 4), // "type"
QT_MOC_LITERAL(4, 36, 7), // "message"
QT_MOC_LITERAL(5, 44, 8), // "dispatch"
QT_MOC_LITERAL(6, 53, 9), // "loadClass"
QT_MOC_LITERAL(7, 63, 13) // "javaClassName"

    },
    "QASystemDispatcher\0dispatched\0\0type\0"
    "message\0dispatch\0loadClass\0javaClassName"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QASystemDispatcher[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   34,    2, 0x06 /* Public */,

 // methods: name, argc, parameters, tag, flags
       5,    2,   39,    2, 0x02 /* Public */,
       5,    1,   44,    2, 0x22 /* Public | MethodCloned */,
       6,    1,   47,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QVariantMap,    3,    4,

 // methods: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QVariantMap,    3,    4,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    7,

       0        // eod
};

void QASystemDispatcher::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QASystemDispatcher *_t = static_cast<QASystemDispatcher *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->dispatched((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QVariantMap(*)>(_a[2]))); break;
        case 1: _t->dispatch((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QVariantMap(*)>(_a[2]))); break;
        case 2: _t->dispatch((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->loadClass((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QASystemDispatcher::*_t)(QString , QVariantMap );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QASystemDispatcher::dispatched)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject QASystemDispatcher::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QASystemDispatcher.data,
      qt_meta_data_QASystemDispatcher,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QASystemDispatcher::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QASystemDispatcher::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QASystemDispatcher.stringdata0))
        return static_cast<void*>(const_cast< QASystemDispatcher*>(this));
    return QObject::qt_metacast(_clname);
}

int QASystemDispatcher::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void QASystemDispatcher::dispatched(QString _t1, QVariantMap _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
