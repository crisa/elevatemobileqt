import QtQuick 2.4
import QtQuick.Controls 1.2
import QuickAndroid 0.1

import './drawable'
Item {
  id: root

  property bool isOpened: false
  default property alias content : dragArea.children
  property alias abovePaper: abovePaper.children
  property bool persistent: false
  property alias paper: paper
  property int visibleOverhang: 0
  property bool anchorRight: false
  property alias visualWidth:paper.width
  property int closeThreshold: 75 * A.dp
  property alias xAnchor:paper.x

  signal closed;
  signal opened;

  focus:true
  visible:false
  anchors.fill:parent

  clip: false
  z: Constants.zPopupLayer-1

  Keys.onReleased: {
    if (event.key === Qt.Key_Back ||
        event.key === Qt.Key_Escape) {
      if(isOpened){
        isOpened = false;
        event.accepted = true;
      }
    }
  }

  onPersistentChanged: {
  }

  function open() {
    isOpened = true;
  }

  function close() {
    isOpened = false;
  }

  function toggle() {
    isOpened = !isOpened;
  }

  onIsOpenedChanged: {
    if(isOpened){
      root.forceActiveFocus();
    }
  }


  Mask {
    anchors.top: parent.top
    anchors.left: anchorRight ? parent.left:paper.right
    anchors.right: anchorRight ? paper.left:parent.right
    anchors.bottom: parent.bottom
    MouseArea {
      anchors.fill: parent
      onPressed: close();
    }
    active: !persistent && isOpened
    enabled: isOpened && !persistent
  }
  Item {
    id: abovePaper
    y:paper.y
    x: paper.x
    width:paper.width
    height:childrenRect.height
    z:Constants.zPopupLayer+1
  }

  Paper {
    id: paper
    width: 280 * A.dp
    height: root.height
    x: anchorRight ? root.width - visibleOverhang:-paper.width + visibleOverhang;
    depth:4
    property int targetX: 0

    function __onDragged() {
      if(anchorRight){
        if(isOpened){
          if (paper.x >= root.width-paper.width+root.closeThreshold) {
            close();
          } else {
            close();
            open();
          }
        } else {
          if (paper.x <= root.width - visibleOverhang*2) {
            open();
          } else {
            //trigger default state

            close();
          }
        }
      } else {
        if(isOpened){
          if (paper.x <= -root.closeThreshold) {
            close();
          } else {
            //trigger open state
            close();
            open();
          }
        } else {
          if (paper.x >= -paper.width - visibleOverhang*2) {
            open();
          } else {
            //trigger default state
            open();
            close();
          }
        }
      }
    }

    MouseArea {
      // Dirty hack for a bug in Qt
      // p.s You can't reproduce on desktop. Use Android.
      anchors.fill: parent
      enabled: !persistent
      drag.axis: Drag.XAxis
      drag.target: paper
      drag.minimumX: anchorRight?parent.width:-paper.width
      drag.maximumX: 0


      drag.onActiveChanged: {
        if (!drag.active) {
          paper.__onDragged();
        }
      }
    }

    MouseArea {
      id: dragArea
      width: paper.width
      height: paper.height
      drag.axis: Drag.XAxis
      drag.target: paper
      drag.filterChildren: true
      drag.minimumX: anchorRight? root.width-paper.width:-paper.width
      drag.maximumX: anchorRight? root.width-visibleOverhang:0;
      onPressed: {
        root.forceActiveFocus();
      }
      drag.onActiveChanged: {
        if (!drag.active) {
          paper.__onDragged();
        }
      }
    }
  }



  states : [
    State {
      name: "OPEN"
      when: isOpened
      PropertyChanges {
        target: paper
        x: anchorRight?root.width-paper.width : 0;
        depth:4
      }
    }
  ]

  transitions: [
    Transition {
      from: "*"
      to: "*"
      NumberAnimation {
        target: paper
        properties: "x"
        duration: 200
        easing.type: Easing.InOutQuad
      }
      onRunningChanged: {
        if(running==false){
          if(!isOpened){
            root.visible=false;
            closed();
          }
        } else if( running == true){
          if(isOpened){
            root.visible=true;
            opened();
          }
        }
      }
    }

  ]
}

