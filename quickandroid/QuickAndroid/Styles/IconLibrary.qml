import QtQuick 2.4
pragma Singleton

Item {
    readonly property string fontFamily: "Material Icons"

    FontLoader {
        source: "../fonts/MaterialIcons-Regular.ttf"
    }

    readonly property string menu: "\ue5d2"
    readonly property string moreHoriz: "\ue5d3"
    readonly property string moreVert: "\ue5d4"
    readonly property string refresh: "\ue5d5"
    readonly property string sync: "\ue627"
    readonly property string save: "\ue161"
    readonly property string trash: "\ue872"
    readonly property string edit: "\ue3c9"
    readonly property string camera: "\ue412"
    readonly property string photo: "\ue410"
    readonly property string search: "\ue8b6"
    readonly property string plus: "\ue145"
    readonly property string backArrow: "\ue5c4"
    readonly property string forwardArrow: "\ue5c8"
    readonly property string close: "\ue5cd"
    readonly property string mapLayer: "\ue53b"
    readonly property string people: "\ue7fb"
    readonly property string expand: "\ue5cf"
    readonly property string collapse: "\ue5ce"
    readonly property string gps: "\ue1b3"
    readonly property string clear: "\ue14c"
    readonly property string place: "\ue55f"
    readonly property string rightArrow: "\ue315"
    readonly property string map: "\ue55b"
}

