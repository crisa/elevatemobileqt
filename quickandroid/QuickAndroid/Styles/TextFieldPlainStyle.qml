import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3
import QuickAndroid 0.1
import QuickAndroid.Private 0.1

TextFieldStyle {

    padding.top: 0 * A.dp
    padding.bottom: 20 * A.dp
    padding.left: 20 * A.dp
    padding.right: 0

    textColor: control.enabled ? control.textColor : Qt.lighter(control.textColor,150);

    placeholderTextColor: control.placeholderTextColor

    background: Item {

    }



}

