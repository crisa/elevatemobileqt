import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3
import QuickAndroid 0.1
import QuickAndroid.Private 0.1
import QtGraphicalEffects 1.0
TextFieldPlainStyle {

    id: style

    objectName: "TextFieldStyleInstance"
    //padding.top: (control.hasFloatingLabel ? 40 * A.dp : 16 * A.dp) + control._fontDiff - 2
    //padding.bottom: 16 * A.dp



    textColor:  control.enabled ? control.textColor : control.material.text.disabledTextColor

    TextMetrics {
        id: textMetrics
        font: control.font
        text: "012345678"
    }

    background: Item {
        id: bg
        objectName: "Background"

        property int floatingLabelBottomMarginOnTop : 16 * A.dp + textMetrics.height + 8 * A.dp



        Rectangle {
            radius: Units.dp(3)
            id: activeUnderline
            height: parent.height
            border.color: "#cccccc"
            border.width: Units.dp(1)
            color: "#ffffff"
            width: parent.width
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 8 * A.dp
            anchors.horizontalCenter: parent.horizontalCenter

        }

        MaterialShadow {
            asynchronous: false
            anchors.fill: activeUnderline
            depth: Units.dp(2)
            opacity: 0.2
        }

        Text {
            id: floatingLabelTextItem
            objectName: "FloatingLabelText"
            font.family: "Fakt Nor"
            height: parent.height
            font.pixelSize: Units.dp(26)
            color: "#ff0000"
            anchors.leftMargin: 20
            text: control.floatingLabelText


        }

        /*
        states: [
            State {
                name: "FloatingLabelOnTop"
                when: control.hasFloatingLabel && (control.activeFocus || control.text !=="")

                PropertyChanges {
                    target: floatingLabelTextItem
                    font.pixelSize: 12 * A.dp
                    color: control.color;
                    anchors.bottomMargin: bg.floatingLabelBottomMarginOnTop
                }
            },
            State {
                name: "HasFloatingLabel"
                when: control.hasFloatingLabel

                PropertyChanges {
                    target: style
                    placeholderTextColor : Constants.transparent
                }
            }
        ]
        */
/*
        transitions: [
            Transition {
                from: "*"
                to: "FloatingLabelOnTop"
                reversible: true

                NumberAnimation {
                    target: floatingLabelTextItem
                    properties: "anchors.bottomMargin,font.pixelSize"
                    duration: 200
                    easing.type: Easing.InOutQuad
                }

                ColorAnimation {
                    targets: floatingLabelTextItem
                    properties: "color"
                    duration: 200
                    easing.type: Easing.InOutQuad
                }

                ColorAnimation {
                    targets: style
                    properties: "placeholderTextColor"
                    duration: 200
                    easing.type: Easing.InOutQuad
                }
            }
        ]
        */
    }
}

