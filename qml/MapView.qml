import QtQuick 2.7
import Esri.ArcGISRuntime 100.0
import Esri.ArcGISExtras 1.1
import QuickAndroid 0.1
import QuickAndroid.Styles 0.1
import QtPositioning 5.2
import QtSensors 5.3
import "./js/request.js" as Http
import "."


Page {
    id:root
    objectName: "MapView"
    actionBar: ActionBar {
      id : actionBar
      title: Services.applicationDetails.applicationName
      showIcon: true
      onActionButtonClicked: back();
      actionButtonEnabled: true
      menuBar: Button {
        width:Units.dp(48)
        height : actionBar.height
        background: actionBar.material.actionButtonBackground
        clip : true
      }
    }
    MapView {
        id:mapView
        anchors.fill: parent
        Map {
           id:map
           ArcGISTiledLayer{
                id:basemap;
                url:Services.applicationConfigs.map.baseMapLayers[0].url.replace("https://s3.amazonaws.com/cache.39dn.com/","http://s3pngrouterproxy.herokuapp.com/")
           }
           ArcGISMapImageLayer{
                id:mapService
                url:Services.applicationConfigs.map.operationalLayers[0].url
           }
           onLoadStatusChanged:  {
               if(loadStatus === Enums.LoadStatusLoaded){
                 //mapView.setViewpointGeometry(GeometryEngine.project(fullExtent.geometry,SpatialReference.createWebMercator()));
                 mapView.setViewpointGeometry(fullExtent.geometry);
               }
        }


        EnvelopeBuilder {
          id: fullExtent
          yMax: Services.applicationConfigs.map.extent.ymax
          xMax: Services.applicationConfigs.map.extent.xmax
          yMin: Services.applicationConfigs.map.extent.ymin
          xMin: Services.applicationConfigs.map.extent.xmin
        }

        }
    }
    TextField {

      anchors.right:parent.right
      visible: true
      width: parent.width-Units.dp(20)
      anchors.top:parent.top
      anchors.margins: Units.dp(10)
      anchors.topMargin: Units.dp(20)

      placeholderText: "Search Elevate"
      placeholderTextColor: "#d8d8d8"
      Text {
          id:searchIcon
          width: Units.dp(56)
          x:parent.width - searchIcon.width
          anchors.bottomMargin:Units.dp(5)
          font.family: IconLibrary.fontFamily
          font.pixelSize: Units.dp(30)
          text: IconLibrary.search
          color:"#d8d8d8"
          horizontalAlignment: Text.AlignHCenter

      }


    }
    Component.onCompleted: {
        //ArcGISRuntimeEnvironment.createObject("Envelope",{});
    }
}
