pragma Singleton
import QtQuick 2.4

QtObject {
    property string black : "#000000"
    property string black100 : "#000000"
    property string black87 : "#de000000"
    property string black54 : "#8a000000"
    property string black12 : "#1a000000"

    property string white: "#ffffff"
    property string white100: "#ffffff"
    property string white87 : "#deffffff"
    property string white54 : "#8affffff"
    property string white38 : "#61ffffff"

    property string transparent : "#00000000"

    // The z value of popup layer

    property int zPopupLayer :      100000000
    property int zInverseMouseArea: 200000000
}
