pragma Singleton
import QtQuick 2.7;
import QtQuick.Controls 2.1;


Item {

    property color mainAppColor: "#cea936"
    property color listItemActiveColor: "#cea936"
    property color listItemHighlightColor: "#ededed"
    property color textButtonColor: "#5c6bc0"
    property string defaultFontFamily: "Fakt Pro Nor"
    property string defaultFontFamilyMedium: "Fakt Pro Med"

    FontLoader {
        source: "qrc:/Resources/fonts/FaktPro-Normal.ttf"
    }

    FontLoader {
        source: "qrc:/Resources/fonts/FaktPro-Medium.ttf"
    }

}
