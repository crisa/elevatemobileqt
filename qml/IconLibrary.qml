pragma Singleton
import QtQuick 2.7

Item {

    readonly property string fontFamily: "Material Icons"
    readonly property string tdnFontFamily: "icons"

    FontLoader {
        source: "qrc:/Resources/icons/MaterialIcons-Regular.ttf"
    }
    FontLoader {
        source: "qrc:/Resources/icons/icons.ttf"
    }
    readonly property string menu: "\ue5d2"
    readonly property string search: "\ue8b6"
    readonly property string backArrow: "\ue5c4"
    readonly property string forwardArrow: "\ue409"
    readonly property string close: "\ue5cd"
    readonly property string mapLayer: "\ue53b"
    readonly property string people: "\ue7fb"
    readonly property string expand: "\ue5cf"
    readonly property string collapse: "\ue5ce"

}

