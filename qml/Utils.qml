pragma Singleton
import QtQuick 2.7

QtObject {

  function joinWithSlash(str1,str2,_numSlashes){
    var numSlashes = parseInt(_numSlashes) || 1;
    return String(str1).replace(/\/+$/,'') + (new Array(numSlashes+1).join("/")) + String(str2).replace(/^\/+/,"");
  }

}
