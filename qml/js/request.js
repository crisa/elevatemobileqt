.pragma library


function get(url, callback) {
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = (function(myxhr) {
    return function() {
      if(myxhr.readyState === 4){
        callback(myxhr);
      }
    }
  })(xhr);
  xhr.open('GET', url, true);
  xhr.send('');
}


function addAccessToken(url, token){

  if((typeof url === "string" || url instanceof String)&&(typeof token === "string" || token instanceof String)){

    if(url.indexOf("access_token=") < 0){
      if(url.indexOf("?")>-1){
        url += "&access_token=" + token;
      } else {
        url += "?access_token=" + token;
      }
    }

    return url;
  }

  return false;
}

function post(url, body, callback) {
  var xhr = new XMLHttpRequest();

  xhr.onreadystatechange = (function(myxhr) {
    return function() {
      if(myxhr.readyState === xhr.DONE){

        callback(myxhr);
      }
    }
  })(xhr);

  xhr.open('post', url, true);
  xhr.setRequestHeader("Content-Type","application/json");
  xhr.setRequestHeader("Accept","*/*");
  xhr.setRequestHeader("Origin","https://monroein.elevatemaps.io");
  xhr.send(body);
}
