pragma Singleton
import QtQuick 2.7
import "./js/request.js" as Http
import "." //necessary to register other singletons due to bug

QtObject {
  id:root
  property string token: ""
  property var applications: []
  property var applicationDetails:null
  property var applicationConfigs:null

   function sortArray(arr, prop, order, type) {
        type = type || "alphabetic";
        order = order || "ascending";

        switch (type) {
          case "object":
            var props = prop.split(";");

            if (order == "ascending") {
              arr.sort(function (a, b) {
                if (a[props[0]][props[1]] < b[props[0]][props[1]]) {
                  return 1;
                }

                if (a[props[0]][props[1]] > b[props[0]][props[1]]) {
                  return -1;
                }

                return 0;
              });
            } else {
              arr.sort(function (a, b) {
                if (a[props[0]][props[1]] < b[props[0]][props[1]]) {
                  return -1;
                }

                if (a[props[0]][props[1]] > b[props[0]][props[1]]) {
                  return 1;
                }

                return 0;
              });
            }
            break;

          case "alphabetic":
            if (order == "ascending") {
              arr.sort(function (a, b) {
                if (a[prop] == b[prop])
                  return 0;
                if (a[prop] > b[prop])
                  return 1;
                else
                  return -1
              });
            }
            else {
              arr.sort(function (a, b) {
                if (a[prop] == b[prop])
                  return 0;
                if (a[prop] < b[prop])
                  return 1;
                else
                  return -1
              });
            }
            break;

          default:
            if (order == "ascending") {
              arr.sort(function (a, b) {
                if (parseFloat(a[prop]) == parseFloat(b[prop]))
                  return 0;
                if (parseFloat(a[prop]) > parseFloat(b[prop]))
                  return 1;
                else
                  return -1
              });
            }
            else {
              arr.sort(function (a, b) {
                if (parseFloat(a[prop]) == parseFloat(b[prop]))
                  return 0;
                if (parseFloat(a[prop]) < parseFloat(b[prop]))
                  return 1;
                else
                  return -1
              });
            }

            if (has("ie")) {
              arr = arr.reverse();
            }
            break;
        }
      }

  function getApplications(callback){
      Http.get(Config.apiApplicationEndpoint+Config.apiKey,function(response){
         root.applications = JSON.parse(response.responseText).data;
         sortArray(root.applications,"applicationName","ascending","alphabetic");
         return callback(root.applications);
      })
  }

  function getApplicationDetails(applicationId,callback){
      Http.get(Config.apiApplicationDetails+applicationId+Config.apiKey,function(response){
         root.applicationDetails = JSON.parse(response.responseText);
         return callback(root.applicationDetails);
      })
  }

  function getApplicationConfigs(callback){
      Http.get(Config.apiApplicationConfigs.replace("{applicationId}",root.applicationDetails._id)+Config.apiKey,function(response){
         root.applicationConfigs = JSON.parse(response.responseText);
         return callback(root.applicationConfigs);
      })
  }
}
