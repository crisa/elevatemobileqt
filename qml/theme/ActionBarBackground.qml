import QtQuick 2.0
import QuickAndroid 0.1

Item {

    MaterialShadow {
        asynchronous: true
        anchors.fill: parent
        depth: 1
    }

    Rectangle {
        color: "#33aae4"
        anchors.fill: parent
    }
}

