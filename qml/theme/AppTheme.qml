import QtQuick 2.0
import QuickAndroid 0.1
import QuickAndroid.Styles 0.1

pragma Singleton

Theme {
    mediumText.textSize: 18 * A.dp
    smallText.textSize : 14 * A.dp

    property color signInBgColor: "#33aae4"

    property TextFieldMaterial signInTextMaterial: TextFieldMaterial {
        id: style

        // The style of input text
        property TextMaterial text : TextMaterial {
          textSize: 24 * A.dp
          textColor: Constants.white87
          disabledTextColor: Constants.white87
        }

        // The color of underline and floating text label on active.
        property string color

        property string disabledColor : Constants.white54

        property string inactiveColor : Constants.white54

        property string placeholderTextColor: Constants.white54
    }

    colorPrimary: "#3f51b5"
    textColorPrimary: Constants.black87
    windowBackground: "#eeeeee";

    // The default icon of ActionBar is a "back" image
    actionBar.iconText: IconLibrary.backArrow
    actionBar.iconColor: "#FFFFFF"

    // Background with shadow
    actionBar.background: ActionBarBackground {
    }


    actionBar.title : TextMaterial {
        textSize: 18 * A.dp
        textColor : Constants.white87
    }

    actionBar.iconSize: 24 * A.dp

    property TextMaterial header1: TextMaterial {
        textSize: 24 * A.dp
        textColor : Constants.black87
    }

    property TextMaterial subHeader1: TextMaterial {
        textSize: 14 * A.dp
        textColor : Constants.black54
    }

    property RaisedButtonMaterial raisedButton1: RaisedButtonMaterial {
        backgroundColor: "#ff4081"
        depth:1
        colorPressed: "#FF99BB"
        text: TextMaterial {
            id:raisedButton1TextMaterial
            textSize: 14 * A.dp
            textColor: Constants.white100
            disabledTextColor: Constants.white54
        }
    }


}

