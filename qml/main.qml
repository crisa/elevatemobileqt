
// Copyright 2016 ESRI
//
// All rights reserved under the copyright laws of the United States
// and applicable international laws, treaties, and conventions.
//
// You may freely redistribute and use this sample code, with or
// without modification, provided you include the original copyright
// notice and use restrictions.
//
// See the Sample code usage restrictions document for further information.
//

import QtQuick 2.6
import QtQuick.Window 2.0
import QtQuick.Controls 1.4
import Esri.ArcGISRuntime 100.0
import QuickAndroid 0.1
import QuickAndroid.Styles 0.1

import "./theme"
import "." //necessary to register singletons due to bug
Window {
    id: appWindow
    width: 700
    height: 950
    title: "ElevateQT"

    QtObject {
      id:shared
      property var applicationId:"lala";
       property var userCred: userCred;
    }

    Loader {
      id: loader
      anchors.fill:parent
      asynchronous: true
      focus: true;
      sourceComponent: PageStack {
        objectName: "PageStack";
        exposedProperties: shared
        initialPage: Qt.resolvedUrl("CountyChooserView.qml")
      }
    }
    Component.onCompleted: {
      //ThemeManager is a singleton accessible in any item that imports import QuickAndroid.Styles
      ThemeManager.currentTheme = AppTheme;
    }

}
