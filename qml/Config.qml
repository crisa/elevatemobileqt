pragma Singleton
import QtQuick 2.7


QtObject {
  readonly property string apiBaseUrl: "https://manager.elevatemaps.io/api/";
  readonly property string apiKey: "?apikey=3ef255f60c92dafc51009b9b9adf29460cecce7782077bf1";
  readonly property string apiAuthEndpoint: apiBaseUrl + "authenticate";
  readonly property string apiUserEndpoint: apiBaseUrl + "users";
  readonly property string apiApplicationDetails:apiBaseUrl+"applications/fullinfo/app-id/";
  readonly property string apiApplicationConfigs:apiBaseUrl+"configs/application/{applicationId}/appConfig";
  readonly property string apiApplicationEndpoint: apiBaseUrl + "applications";

  readonly property string defaultBasemapUrl: "https://static.arcgis.com/attribution/World_Street_Map"


}
