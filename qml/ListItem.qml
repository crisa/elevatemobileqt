import QtQuick 2.7
import Esri.ArcGISRuntime 100.0
import Esri.ArcGISExtras 1.1
import QuickAndroid 0.1
import QuickAndroid.Styles 0.1
import QtPositioning 5.2

import QtSensors 5.3
import "./js/request.js" as Http
import "."

Item {
    id: root
    property bool selected: false
    property int sideMargins: Units.dp(16)
    property int iconColumnWidth: Units.dp(56)
    property int iconSize: Units.dp(30)
    property int itemHeight: Units.dp(48)
    property alias rightIcon: rightIconColumn.text
    property alias leftIcon: leftIconColumn.text
    property alias text: centerColumn.text
    property string mode: "" // expandable, toggle, button, info
    readonly property var modes: ({info:"info",button:"button",toggle:"toggle",expandable:"expandable"})
    property bool selectedOverride: false // set to true if you want to handle "selected" flag externally
    property Component expandItem

    property bool disabled: false

    signal clicked()

    function unSelect(){
        root.selected = false;
    }

    height:childrenRect.height
    width:parent.width
    Rectangle {
        id: listItem
        height:Units.dp(70)
        width:parent.width
        state: root.disabled ? "DISABLED" : "NORMAL"
        CustomBorder{
            commonBorder: true
            commonBorderWidth:Units.dp(2)
            lBorderwidth: 0
            rBorderwidth: 0
            tBorderwidth: 0
            bBorderwidth:Units.dp(2)
            borderColor: "#e9e9e9"
        }

        Column {
            width:parent.width
            anchors.verticalCenter:parent.verticalCenter
            Item {
                id:mainContents
                height:itemHeight
                width:parent.width
                Text {
                    id:leftIconColumn
                    width: Units.dp(16)
                    x:sideMargins
                    anchors.verticalCenter: parent.verticalCenter
                    font.family: IconLibrary.fontFamily
                    font.pixelSize: iconSize
                    text: leftIcon
                }
                Text {
                    id:centerColumn
                    width:parent.width - sideMargins - rightIconColumn.width - leftIconColumn.width
                    x: sideMargins + leftIconColumn.width
                    anchors.verticalCenter: parent.verticalCenter
                    font.family:AppSettings.defaultFontFamily
                    font.pixelSize: Units.dp(19)
                }
                Text {
                    id:rightIconColumn
                    width: iconColumnWidth
                    x:parent.width - rightIconColumn.width
                    anchors.verticalCenter: parent.verticalCenter
                    font.family: IconLibrary.fontFamily
                    font.pixelSize: iconSize
                    text: rightIcon
                    horizontalAlignment: Text.AlignHCenter
                }
                MouseArea {
                    id: button
                    enabled: root.mode !== "info"
                    anchors.fill:parent
                    onClicked: {
                        if(!selectedOverride){
                            root.selected = !root.selected;
                        }
                        root.clicked();
                    }
                }
            }
            Loader {
                /*
                property int heightBeforeCollapse;
                property bool expanded: false;
                onExpandedChanged: {
                    if(expanded){
                        visible = true;
                        height = heightBeforeCollapse?heightBeforeCollapse:0
                    } else {
                        height = 0;
                    }
                }
                onHeightChanged: {
                    if(height>0){
                        heightBeforeCollapse = height;
                    }
                }
                */

                visible:false
                id:expandItemLoader
                width:parent.width
                sourceComponent: root.expandItem


            }
        }

        states: [
            State {
                name: "NORMAL"
                PropertyChanges { target: listItem; color: Constants.white}
            },
            State {
                name: "SELECTED"
                PropertyChanges { target: listItem; color: Constants.black12}
            },
            State {
                name: "DISABLED"
                PropertyChanges { target: centerColumn; color: Constants.black12}
                PropertyChanges { target: leftIconColumn; color: Constants.black12}
                PropertyChanges { target: root; enabled:false}
            }
        ]


    }

    /*
    Behavior on height {
        NumberAnimation {
            duration: 200
            easing.type: Easing.InOutQuad
        }
    }
    */

    onSelectedChanged:{
        handleState();
    }

    function handleState(){
        if(root.selected){
            if(root.mode===root.modes.toggle){
                listItem.state = "SELECTED";
            } else if (root.mode===root.modes.expandable){
                listItem.state = "EXPANDED";
            }
        } else {

            if (root.mode===root.modes.expandable){
                listItem.state = "COLLAPSED";
            } else {
                listItem.state = "NORMAL";
            }
        }
    }


}
