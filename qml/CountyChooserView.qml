import QtQuick 2.7
import Esri.ArcGISRuntime 100.0
import Esri.ArcGISExtras 1.1
import QuickAndroid 0.1
import QuickAndroid.Styles 0.1
import QtPositioning 5.2

import QtSensors 5.3
import "./js/request.js" as Http
import "."


Page {
    id:root
    objectName: "CountyChooserView"

    property real scaleFactor: System.displayScaleFactor

    actionBar: ActionBar {
      id : actionBar
      title: "Choose County"
      showIcon: false
      actionButtonEnabled: false
      menuBar: Button {
        width:Units.dp(48)
        height : actionBar.height
        background: actionBar.material.actionButtonBackground
        clip : true
      }
    }

    ListView {
        id:listView
        width: parent.width
        height: parent.height
        model: applicationsModel;
        clip:true
        delegate: ListItem {
            rightIcon:IconLibrary.forwardArrow
            text:applicationName
            onClicked: {
                Services.getApplicationDetails(applicationId,function(applicationDetails){
                    stack.exposedProperties.applicationId=applicationId;
                    Services.getApplicationConfigs(function(applicationConfigs){
                      stack.push(Qt.resolvedUrl("MapView.qml"));
                    })

                })
            }
        }
        footerPositioning: ListView.InlineFooter
        maximumFlickVelocity: Units.dp(3500);
        cacheBuffer: Units.dp(1000);
        ListModel {
            id: applicationsModel;
             ListElement {
                applicationName: "";
                applicationId: "";

            }
        }
    }
    Component.onCompleted: {
        applicationsModel.clear();
        Services.getApplications(function(applications){
            for(var i=0; i<applications.length; i++){
                if(applications[i].applicationName!="eCOP"){
                    applicationsModel.append(applications[i]);
                }
            }
        })
    }
}
