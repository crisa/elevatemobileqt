/****************************************************************************
** Meta object code from reading C++ file 'qaline.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "quickandroid/qaline.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qaline.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QALine_t {
    QByteArrayData data[16];
    char stringdata0[171];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QALine_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QALine_t qt_meta_stringdata_QALine = {
    {
QT_MOC_LITERAL(0, 0, 6), // "QALine"
QT_MOC_LITERAL(1, 7, 18), // "orientationChanged"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 12), // "colorChanged"
QT_MOC_LITERAL(4, 40, 15), // "penWidthChanged"
QT_MOC_LITERAL(5, 56, 15), // "penStyleChanged"
QT_MOC_LITERAL(6, 72, 11), // "orientation"
QT_MOC_LITERAL(7, 84, 8), // "penStyle"
QT_MOC_LITERAL(8, 93, 8), // "PenStyle"
QT_MOC_LITERAL(9, 102, 5), // "color"
QT_MOC_LITERAL(10, 108, 8), // "penWidth"
QT_MOC_LITERAL(11, 117, 9), // "SolidLine"
QT_MOC_LITERAL(12, 127, 8), // "DashLine"
QT_MOC_LITERAL(13, 136, 7), // "DotLine"
QT_MOC_LITERAL(14, 144, 11), // "DashDotLine"
QT_MOC_LITERAL(15, 156, 14) // "DashDotDotLine"

    },
    "QALine\0orientationChanged\0\0colorChanged\0"
    "penWidthChanged\0penStyleChanged\0"
    "orientation\0penStyle\0PenStyle\0color\0"
    "penWidth\0SolidLine\0DashLine\0DotLine\0"
    "DashDotLine\0DashDotDotLine"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QALine[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       4,   38, // properties
       1,   54, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x06 /* Public */,
       3,    0,   35,    2, 0x06 /* Public */,
       4,    0,   36,    2, 0x06 /* Public */,
       5,    0,   37,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       6, QMetaType::Int, 0x00495103,
       7, 0x80000000 | 8, 0x0049510b,
       9, QMetaType::QColor, 0x00495103,
      10, QMetaType::QReal, 0x00495103,

 // properties: notify_signal_id
       0,
       3,
       1,
       2,

 // enums: name, flags, count, data
       8, 0x0,    5,   58,

 // enum data: key, value
      11, uint(QALine::SolidLine),
      12, uint(QALine::DashLine),
      13, uint(QALine::DotLine),
      14, uint(QALine::DashDotLine),
      15, uint(QALine::DashDotDotLine),

       0        // eod
};

void QALine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QALine *_t = static_cast<QALine *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->orientationChanged(); break;
        case 1: _t->colorChanged(); break;
        case 2: _t->penWidthChanged(); break;
        case 3: _t->penStyleChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QALine::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QALine::orientationChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QALine::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QALine::colorChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QALine::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QALine::penWidthChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QALine::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QALine::penStyleChanged)) {
                *result = 3;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QALine *_t = static_cast<QALine *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->orientation(); break;
        case 1: *reinterpret_cast< PenStyle*>(_v) = _t->penStyle(); break;
        case 2: *reinterpret_cast< QColor*>(_v) = _t->color(); break;
        case 3: *reinterpret_cast< qreal*>(_v) = _t->penWidth(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        QALine *_t = static_cast<QALine *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setOrientation(*reinterpret_cast< int*>(_v)); break;
        case 1: _t->setPenStyle(*reinterpret_cast< PenStyle*>(_v)); break;
        case 2: _t->setColor(*reinterpret_cast< QColor*>(_v)); break;
        case 3: _t->setPenWidth(*reinterpret_cast< qreal*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

const QMetaObject QALine::staticMetaObject = {
    { &QQuickPaintedItem::staticMetaObject, qt_meta_stringdata_QALine.data,
      qt_meta_data_QALine,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QALine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QALine::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QALine.stringdata0))
        return static_cast<void*>(const_cast< QALine*>(this));
    return QQuickPaintedItem::qt_metacast(_clname);
}

int QALine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickPaintedItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QALine::orientationChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QALine::colorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QALine::penWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QALine::penStyleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
