/****************************************************************************
** Meta object code from reading C++ file 'qadevice.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "quickandroid/qadevice.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qadevice.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QADevice_t {
    QByteArrayData data[10];
    char stringdata0[76];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QADevice_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QADevice_t qt_meta_stringdata_QADevice = {
    {
QT_MOC_LITERAL(0, 0, 8), // "QADevice"
QT_MOC_LITERAL(1, 9, 9), // "dpChanged"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 10), // "dpiChanged"
QT_MOC_LITERAL(4, 31, 15), // "isTabletChanged"
QT_MOC_LITERAL(5, 47, 9), // "osChanged"
QT_MOC_LITERAL(6, 57, 2), // "dp"
QT_MOC_LITERAL(7, 60, 3), // "dpi"
QT_MOC_LITERAL(8, 64, 8), // "isTablet"
QT_MOC_LITERAL(9, 73, 2) // "os"

    },
    "QADevice\0dpChanged\0\0dpiChanged\0"
    "isTabletChanged\0osChanged\0dp\0dpi\0"
    "isTablet\0os"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QADevice[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       4,   38, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x06 /* Public */,
       3,    0,   35,    2, 0x06 /* Public */,
       4,    0,   36,    2, 0x06 /* Public */,
       5,    0,   37,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       6, QMetaType::QReal, 0x00495001,
       7, QMetaType::QReal, 0x00495001,
       8, QMetaType::Bool, 0x00495001,
       9, QMetaType::QString, 0x00495001,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,

       0        // eod
};

void QADevice::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QADevice *_t = static_cast<QADevice *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->dpChanged(); break;
        case 1: _t->dpiChanged(); break;
        case 2: _t->isTabletChanged(); break;
        case 3: _t->osChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QADevice::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QADevice::dpChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QADevice::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QADevice::dpiChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QADevice::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QADevice::isTabletChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QADevice::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QADevice::osChanged)) {
                *result = 3;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        QADevice *_t = static_cast<QADevice *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< qreal*>(_v) = _t->dp(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->dpi(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->isTablet(); break;
        case 3: *reinterpret_cast< QString*>(_v) = _t->os(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

const QMetaObject QADevice::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QADevice.data,
      qt_meta_data_QADevice,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QADevice::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QADevice::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QADevice.stringdata0))
        return static_cast<void*>(const_cast< QADevice*>(this));
    return QObject::qt_metacast(_clname);
}

int QADevice::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QADevice::dpChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QADevice::dpiChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void QADevice::isTabletChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void QADevice::osChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
