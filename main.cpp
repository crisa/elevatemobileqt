
// Copyright 2016 ESRI
//
// All rights reserved under the copyright laws of the United States
// and applicable international laws, treaties, and conventions.
//
// You may freely redistribute and use this sample code, with or
// without modification, provided you include the original copyright
// notice and use restrictions.
//
// See the Sample code usage restrictions document for further information.
//

#include <QDebug>
#include <QSettings>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickWindow>
#include <QCommandLineParser>
#include <QNetworkProxy>
#include <QDir>
#include "quickandroid.h"
#include "qadrawableprovider.h"
#include "qasystemdispatcher.h"

/*#ifdef Q_OS_ANDROID
#include <QtAndroidExtras/QAndroidJniObject>
#include <QtAndroidExtras/QAndroidJniEnvironment>

JNIEXPORT jint JNI_OnLoad(JavaVM* vm, void*) {
    Q_UNUSED(vm);
    qDebug("NativeInterface::JNI_OnLoad()");

    // It must call this function within JNI_OnLoad to enable System Dispatcher
    QASystemDispatcher::registerNatives();

    return JNI_VERSION_1_6;
}
#endif
*/



#ifdef Q_OS_WIN
#include <Windows.h>
#endif

#include "AppInfo.h"

//------------------------------------------------------------------------------

#define kSettingsFormat                 QSettings::IniFormat

//------------------------------------------------------------------------------

#define kArgShowName                    "show"
#define kArgShowValueName               "showOption"
#define kArgShowDescription             "Show option maximized | minimized | fullscreen | normal | default"
#define kArgShowDefault                 "show"

#define kShowMaximized                  "maximized"
#define kShowMinimized                  "minimized"
#define kShowFullScreen                 "fullscreen"
#define kShowNormal                     "normal"

//------------------------------------------------------------------------------

int main(int argc, char *argv[])
{
    qDebug() << "Initializing application";



    QGuiApplication app(argc, argv);

    QCoreApplication::setApplicationName(kApplicationName);
    QCoreApplication::setApplicationVersion(kApplicationVersion);
    QCoreApplication::setOrganizationName(kOrganizationName);
#ifdef Q_OS_MAC
    QCoreApplication::setOrganizationDomain(kOrganizationName);
#else
    QCoreApplication::setOrganizationDomain(kOrganizationDomain);
#endif
    QSettings::setDefaultFormat(kSettingsFormat);

#ifdef Q_OS_WIN
    // Force usage of OpenGL ES through ANGLE on Windows
    QCoreApplication::setAttribute(Qt::AA_UseOpenGLES);
    qDebug() << "Setting up network application proxy";
    //QNetworkProxy::setApplicationProxy(QNetworkProxy(QNetworkProxy::HttpProxy, "localhost", 8888));
#endif


#ifdef kLicense
    QCoreApplication::instance()->setProperty("Esri.ArcGISRuntime.license", kLicense);
#endif


    // Intialize application window

    QQmlApplicationEngine appEngine;
    appEngine.addImportPath("qrc:///");
    appEngine.load(QUrl(kApplicationSourceUrl));

    auto topLevelObject = appEngine.rootObjects().value(0);
    qDebug() << Q_FUNC_INFO << topLevelObject;

    auto window = qobject_cast<QQuickWindow *>(topLevelObject);
    if (!window)
    {
        qCritical("Error: Your root item has to be a Window.");

        return -1;
    }

/*#ifdef Q_OS_ANDROID
    // Initialize storagePath
    QAndroidJniObject activity =
            QAndroidJniObject::callStaticObjectMethod(
                "org/qtproject/qt5/android/QtNative",
                "activity", "()Landroid/app/Activity;");
    QAndroidJniObject dirs = activity.callObjectMethod("getExternalFilesDirs", "(Ljava/lang/String;)[Ljava/io/File;", NULL);
    QString dir1String;
    QString dir2String;
    if (dirs.isValid())
    {
        QAndroidJniEnvironment env;
        jsize l = env->GetArrayLength(dirs.object<jarray>());
        if (l>0)
        {
            QAndroidJniObject dir1 = env->GetObjectArrayElement(dirs.object<jobjectArray>(), 0);
            dir1String = dir1.toString();
        }
        if (l>1)
        {
            QAndroidJniObject dir2 = env->GetObjectArrayElement(dirs.object<jobjectArray>(), 1);
            dir2String = dir2.toString();
        }
    }

    // added lines to detect external path
    if(topLevelObject->setProperty("internalStoragePath", dir1String)){
        if (topLevelObject->setProperty("extSdCardPath", dir2String)){
            qInfo("externalsdcard path was set");
        } else
        {
            qCritical("sdcardpath was not set ");
            return -1;
        }
    } else {
        qCritical("internal path was not set ");
        return -1;
    }
#endif
*/

#if !defined(Q_OS_IOS) && !defined(Q_OS_ANDROID)
    // Process command line

    QCommandLineOption showOption(kArgShowName, kArgShowDescription, kArgShowValueName, kArgShowDefault);

    QCommandLineParser commandLineParser;

    commandLineParser.setApplicationDescription(kApplicationDescription);
    commandLineParser.addOption(showOption);
    commandLineParser.addHelpOption();
    commandLineParser.addVersionOption();
    commandLineParser.process(app);

    // Show app window

    auto showValue = commandLineParser.value(kArgShowName).toLower();


    if (showValue.compare(kShowMaximized) == 0)
    {
        window->showMaximized();
    }
    else if (showValue.compare(kShowMinimized) == 0)
    {
        window->showMinimized();
    }
    else if (showValue.compare(kShowFullScreen) == 0)
    {
        window->showFullScreen();
    }
    else if (showValue.compare(kShowNormal) == 0)
    {
        window->showNormal();
    }
    else
    {
        window->show();
    }




#else
    window->show();
#endif

    return app.exec();
}

//------------------------------------------------------------------------------
